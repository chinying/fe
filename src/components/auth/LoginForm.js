import React, { Component } from "react";
import { Button, Checkbox, Form } from 'semantic-ui-react'

import { login } from '../../actions/auth/index'

export default class Login extends Component {
  state = { username: '', password: '' }

  // required to update the input values into `state` variable
  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  handleSubmit = () => {
    const { username, password } = this.state

    // mainpulate the values in this.state for validation

    // call login api
    login(username, password)
  }

  render() {
    const { username, password } = this.state

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group>
            <Form.Input label='Username' name='username' value={username} onChange={this.handleChange}  />
            <Form.Input label='Password' name='password' type='password' value={password} onChange={this.handleChange} />
            <Form.Button content='Submit' />
          </Form.Group>
        </Form>
      </div>
    )
  }
}
