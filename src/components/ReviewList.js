import React, { Component } from 'react';

class ReviewItem extends Component {
  render() {
    const { review } = this.props;
    return (
      <div>
        <h4> {review['title']} </h4>
        <p> {review['body']} </p>
      </div>
    )
  }
}

export default ReviewItem;
