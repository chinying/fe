import React, { Component } from "react";
import { Button, Checkbox, Form } from 'semantic-ui-react'

export default class ReviewForm extends Component {
  state = { title: '', body: '' }

  // required to update the input values into `state` variable
  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  handleSubmit = () => {
    const { title, body } = this.state

    // mainpulate the values in this.state for validation
  }

  render() {
    const { title, body } = this.state

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group>
            <Form.Input label='title' name='title' value={title} onChange={this.handleChange}  />
            <Form.TextArea label='body' name='body' value={body} onChange={this.handleChange}  />
            <Form.Button content='Submit' />
          </Form.Group>
        </Form>
      </div>
    )
  }
}
