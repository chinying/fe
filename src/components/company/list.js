import React, { Component } from 'react';
import { connect } from "react-redux";

const mapStateToProps = state => {
  console.log(state.companies)
  return { companies: state.companies };
};

const ConnectedSet = ({ companies }) => (
  <ul className="list-group list-group-flush">
    {companies.map(el => (
      <li className="list-group-item" key={el.id}>
        {el.title}
      </li>
    ))}
  </ul>
);

const CompanyList = connect(mapStateToProps)(ConnectedSet);

export default CompanyList;
