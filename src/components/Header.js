import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { companies, login_path } from '../constants/paths';

// The Header creates links that can be used to navigate between routes.
const Header = () => (
  <header>
    <nav>
      <ul>
        <li><Link to='/'>Home</Link></li>
        <li><Link to={companies}>Companies</Link></li>
        <li><Link to={login_path}>Login</Link></li>
      </ul>
    </nav>
  </header>
)

export default Header;
