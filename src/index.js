import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';
import configureStore from './stores/configureStore';

import { browse, dashboard, reviews, companies, login_path } from './constants/paths';
import Header from './components/Header';
import ReviewList from './components/ReviewList';
import CompanyList from './components/company/list';
import Login from './components/auth/LoginForm';

import 'semantic-ui-css/semantic.min.css';

const store = configureStore();

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Header />
            <Switch>
              <Route exact path={reviews} component={ ReviewList } />
              <Route exact path={companies} component={ CompanyList } />
              <Route exact path={login_path} component={ Login } />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
registerServiceWorker();
