import { combineReducers } from 'redux';
import {
  routerReducer
} from 'react-router-redux';

// import lists from './lists';
import companies from './companies/';

const rootReducer = combineReducers({
  routing: routerReducer,
  companies,
});

export default rootReducer;
