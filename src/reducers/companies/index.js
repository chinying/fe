import * as actions from '../../constants/company';

const initialState = {
  companies: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case actions.GET_COMPANY_START:
      return getLists(state);
    default:
      return state;
  return state;
  }
}

function getLists(state) {
  return {...state, ...state.companies};
}
