export function login(username, password) {
  console.log("calling fetch api")
  console.log(username, password)

  const myHeaders = new Headers({
    "Content-Type": "application/json",
  });

  var responseHeaders;

  fetch('http://localhost:3000/auth/sign_in', {
    headers: myHeaders,
    method: "POST",
    body: JSON.stringify({"email": username, "password": password})
  })
  .then(response => {
    responseHeaders = response.headers;
    return response.json()
  })
  .then((r) => {
    // response object has success = false if there is error logging in
    if (r["success"] !== false) {
      let accessToken = responseHeaders.get("access-token");
      let client = responseHeaders.get("client");
      let expiry = responseHeaders.get("expiry");
      let uid = responseHeaders.get("uid");
      // TODO: save to store / redux
      storeAuthTokens(accessToken, client, expiry, uid);
    }
  })
  .catch((err) => {
    console.log("ERROR caught: ", err);
  });
}

function storeAuthTokens(token, client, expiry, uid) {
  localStorage.setItem("auth-token", token);
  localStorage.setItem("client", client);
  localStorage.setItem("expiry", expiry);
  localStorage.setItem("uid", uid);
}
