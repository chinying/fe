export const list = () => {

  const myHeaders = new Headers({
    "Content-Type": "application/json",
    "access-token": localStorage.getItem("access-token");
    "client": localStorage.getItem("client");
    "expiry": localStorage.getItem("expiry");
    "uid": localStorage.getItem("uid");
  });


  fetch('localhost:3000/api/reviews/')
  .then(response => response.json())
  .then((r) => {
    console.log(r);
  });
}

export const post = (title, body) => {
  const myHeaders = new Headers({
    "Content-Type": "application/json",
    "access-token": localStorage.getItem("access-token");
    "client": localStorage.getItem("client");
    "expiry": localStorage.getItem("expiry");
    "uid": localStorage.getItem("uid");
  });


  fetch('localhost:3000/companies/', {
    headers: myHeaders
  })
  .then(response => response.json())
  .then((r) => {
    console.log(r);
  });
}
