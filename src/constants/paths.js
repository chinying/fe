export const dashboard = '/dashboard';
export const browse = '/browse';
export const companies = '/companies';
export const reviews = '/reviews';
export const login_path = '/login';
